# OpenML dataset: Ailerons

https://www.openml.org/d/43995

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Dataset used in the tabular data benchmark https://github.com/LeoGrin/tabular-benchmark, transformed in the same way. This dataset belongs to the "classification on numerical features" benchmark. Original description: 
 
**Author**: Luis Torgo","Rui Camacho  
**Source**: Unknown - 2014-08-18  
**Please cite**:   

This data set addresses a control problem, namely flying a F16 aircraft. The attributes describe the status of the aeroplane, while the goal is to predict the control action on the ailerons of the aircraft.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43995) of an [OpenML dataset](https://www.openml.org/d/43995). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43995/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43995/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43995/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

